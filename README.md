# Code as part of a team

In `base.py` is a version of the file that only the Coordinator is allowed
to modify.

Everyone else is only allowed to write functions in files that have the same
name.

The coordinator then copy-pastes all of the work that everyone did into
quantopian and then include a report here in the README.


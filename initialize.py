def initialize(context):
    # Reference to the AAPL security.
    context.aapl = sid(24)
    context.tesla = symbol('TSLA')

    # Rebalance every day, one hour and a half after market open.
    schedule_function(my_rebalance, 
        date_rules.every_day(), 
        time_rules.market_open(hours=1, minutes=30))

